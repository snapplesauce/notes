---
title: self
---

# self

* self - value:
    * true ownership, can pass it but it changes ownership
* &mut self - mutable reference:
    *  can change it but have to replace with any value
* &self: shared reference:
   * temporary shared access to a value
