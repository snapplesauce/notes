
pub struct List {
    head: Link,
}

impl List {
    pub fn new() -> Self {
        List {
             head: Link::Empty }
        }
    }


enum Link {
    Empty,
    More(Box<Node>),
}

struct Node {
    elem: i32,
    next: Link,
}

