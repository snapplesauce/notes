
// 0 is even
// 1 is odd
// for num n, it's evenness is the same as n - 2

function recur_even(number) {
    console.log(number)
    if (number == 0){
      return "even"
    }
    else if (number == 1){
      return "odd"
    }
    else {
      return recur_even(number-2)
    }
}

console.log(recur_even(20))
console.log(recur_even(50))
console.log(recur_even(75))













