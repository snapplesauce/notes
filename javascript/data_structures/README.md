---
title: data_structures
---
# data structures

## arrays

```js
//example
let array = ['a', 'b', 'c']
console.log(array[0]);
// -> 5
```

The index of arrays start at zero.

## properties

Every javascript value except for null and undefined have properties.

* You can access them either using:
    * value.x - x is the literal name of the property
    * value[x] - x is evaluated and then used to access the property

Elements in an array are properties with numbers as the name.

## methods

Properties can hold function values. For instance string.toUppercase() is whats called a method that belongs to the string value.

## objects


