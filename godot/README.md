---
title: godot
---


## **Godot Global(singleton) function  -place in project settings:**


```

     func instance_node(node, location, parent):
         var node_instance = node.instance()

                parent.add_child(node_instance)
                    node_instance.global_position = location

                            return node_instance
```


