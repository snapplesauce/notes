---
title: registers
---

# registers



## 32 bit

- %eax holds system call number
- %ebx holds return status


## 64 bit
  

  
```

  00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 
                                                                 |      |
                                                                  ------
                                                                     |
                                                                    al
                                                        |               |
                                                         ---------------      
                                                                |
                                                               ax
                                       |                                |
                                        --------------------------------                 
                                                         |
                                                        eax
                                                                        
 |                                                                      |
  ----------------------------------------------------------------------
                                        |
                                       rax
                                                         
```




