---
title: registers
---

# Registers

## registers(32 bit)

* EAX- Accumulator:
* ECX- Counter:
* EDX- Data:
* EBX- Base:
* ESP- Stack Pointer:
* EBP- Base Pointer:
* ESI- Source Index:
* EDI- Destination Index:
* EPI- instruction pointer: points at the memory of the instruction the computer will execute
* EFLAGS- consists of bit flags used for various comparisons


## registers(64 bit)
* RAX
* RDI


## r8 -r15
* rX  - 64 bit(  quadword)
* rXd - 32 bit(doubleword)
* rXw - 16 bit(      word)
* rXb - 1  bit(      byte)

