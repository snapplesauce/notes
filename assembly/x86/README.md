---
title: x86
---

# x86 assembly

- the structure of a line = operation <destination>, <source> 
- intel chips
- use gcc -g to add more debug info to compiler

## size

* half word - two bytes
* word - four bytes
* giant - eight bytes

