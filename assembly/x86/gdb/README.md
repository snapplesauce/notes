---
title: gdb
---

# gdb
sudo su -

for 32 bit on 64 bit machine you use gcc -g -m32 -o <out> <in> and add #include <stdlib.h> to the c source code


The config file is ~/.gdbinit.

- Debugger for c code in the linux terminal. 
- Use gcc -g when compiling for more debug info. 
- Use gdb -q executable to run in quiet mode
- Use print <location or register> - to see the location in memory and put it in a temporary variable

## Commands

* b <line number or function name>- add break point specified location.
* list- prints the high level code
* i - displays the memory as human readable assembly instructions
* info functions - displays functions
* disassemble <function> - disassembles the executable and puts breakpoint on function
* x/<number of units><display format><unit size> - examines specified memory location or register
* bt - shows a backtrace of the function calls(stack frames)
* full bt - shows backtrace of the function calls(stack frames) and includes the local variables of the function
* where - shows a backtrace of the stack




## Display Formats

* o - Displays in octal
* x - Displays in hexadecimal
* u - Displays in unsigned based 10
* t - Displays in binary

## Unit Size

* b - one byte
* h - two bytes, a half word
* w - four bytes,     a word
* g - eight bytes ,  a giant

## Pointers

* \* - dereferences **and** declares pointer variables
* \& - called address-of operator, returns the actual memory address where the pointer is stored 

