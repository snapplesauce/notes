


.section .data

.section .text
.globl  _start
_start:
 movl $1, %eax     # syscall for exitting program

 movl $0, %ebx     # returns status to operating system


int $0x80          # wakes up kernel
