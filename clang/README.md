---
title: clang
---
[functions](/functions/README.md)


# C

You need #include <stdio> as the header to access printf.

C relies heavily on libraries.

You ***HAVE*** to include:
```c
int main{

  body
  
  return 0;
}
```
cute the actual c script you write, instead you run it through the program gcc(gcc <script> -o <outpit_filename>), which turns it  into a binary executable.

## Symbolic Constants

```c
#define NAME  value
```

Symbolic constants are ***NOT*** variables! The replacement text can be any sequence of characters.


## Character Input and Output

text stream - A sequence of characters divided into lines, each line followed by a newline character(\n).

* getchar() - reads next input char from text stream
* putchar() - prints a char 

Chars can be represented as intergers. If you have an int c that is a value between {'0'..'0'}






