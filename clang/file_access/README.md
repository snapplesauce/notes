---
title: file_access
---

# file_access

Two ways to access files:
* file descriptors - low level i/o functions
* file streams - higher level buffered i/o built on descriptors

file descriptors are preffered as they are more direct

file descriptors are integers that refference a file

##functions
 
* open(<ptr to filename>, <flags>)
* close(<descriptor>)
* read (<descriptor>, <ptr to data>, <# of bytes>)
* write(<descriptor>, <ptr to data>, <# of bytes>)

##flags

found in fcntl.h:
    - O_RDONLY - open with read only
    - O_WRONLY - open with write only
    - O_APPEND - 
    - O_TRUNC  - open with read and write only
    - O_CREAT  - open with read and write only
  

sys/stat.h









