---
title: command_line_arguments
---

# command_line_arguments

See cmdargs.c

The command line arguments are accessed by passing the main() function an integer(will contain # of args) and a pointer to an array of strings(char *<array name>). The name of the executable is aslways the the first element(array[0]) int the array.

The second argument is always passed in as a string so you have to use conversion functions like ***atoi()***, which stands for ASCII to interger.
