---
title: functions
---

# functions

- You must define functions at the top of the file so they can be found


```C
#include <stdio.h>

int pow(int a, int b);

main()
{
  int i;
  
  for (i = 0; i < 10; ++i) 
      printf("%d %d %d\n", i, power(2,i), power(-3,i));
  return 0;
}

int pow(int base,int exp) 
{
  int i;  p;

    p = 1;
  for (i = 1; i <= exp; ++i) 
      p = p * base;
  return p;
}


```
