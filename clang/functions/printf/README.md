---
title: printf
---
# printf

Function that prints formated strings

## format parameters

expects pointers:
    * %s - String
    * %n - Number of bytes written so far
expects data:
    * %d - Decimal
    * %u - Unsigned decimal
    * %x - Hexadecimal

You can tell printf the field-width you would like the output to display 

```c
unsigned int b = 3025;
printf("[feild-width] 3: '%3u', 10: '%10u', '%08u'\n", b, b, b);
```

