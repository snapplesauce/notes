---
title: misc
---

# misc

- exit(<0 or 1>) - exits program with specified error code
- atoi(<char *>) - ASCII to int takes pointer to string of ASCII and returns an int
- perror(<string>) - prints string with additional error information if available
- malloc(<int>(size)) - allocates memory in the heap
- free(<ptr>) - frees memory on the heap
- strlen(<string>)
- strcpy(<string>)
