---
title: linux
---


## linux
*  shell commands:
    *  ls
    *  pwd
    *  clear
    * cd
    *  cd ..
    * mkdir
    * mv (rename)
    * 2 crl sequences -- CNTRL-C, CNTRL-D
    * touch
    * which
    * alias
    * type + (alias) displays what alias is actually writing
    * \ + (alias) overides alias
    * clip.exe < (filename)   #should try and get xclip to work
    * whatis - displays a brief explanation of program
    * coreurils -- a menu with hyperlinks to GNU programs
    * cat - concatenate files
    * sort - sort lines of text
    * uniq - report or omit repeated lines
    * grep - print lines matching a pattern
    * wc - print newline, word, and byte counts for each file
    * head - output first part of file
    * tail - output last part of file
    * tee - read from standard input and write to standard output and files
    * file descriptor: 0 - stdin, 1 -std out, 2 - error
    * \> - redirects standard output **overrites or creates file**
    * \>\> - redirects standard output but appends to file
    * '#' + redirect - channels to files discriptor (ex: ls /~ 2> error.txt)
    * redirecting both stout and error - ls /~ > text.txt 2>&1 (older)
    * newer way to redirect both stdout and error - ls /~ &> text.txt
    * must redirect error last
    * bit bucket - /dev/null,  redirects get thrown away(like trash can)
    * cat + (file) - copies file into stdin
    * cat with no args waits for you to type, then Ctrl-d indicates eof
    * can redirect(<vor >) cat into a file(stdout) or command line(stdin)
    * | - *pipe operator* pipes stdout of one command into stdin of another
    * (command) | less-  displays output of command that creates stdout
    * \> - connects command with file
    * | - puts output of commandd to input of another
    * grep *pattern* [file...] - finds patterns in text files
    * filter - commands combined together with |
    * tee - reads stdin into file and stdout - ls ~ | tee t.txt | less
    * $ - indicates variables
    * command substitution - ls -l \$(which lynx)
    * history - shows comand history
    * can use ! + history# to repeat that command
    * basename - strips the full path of file down
    * uname - print system information
    * trap arg signals - execute command when a signal is recieved

* Commom signals:
  * SIGHUP  - value 1: signifies hangup
  * SIGINT  - value 2: signifies interupt from keyboard
  * SIGKILL - value 9: signifies kill signal (**Can't be stopped**)
  * SIGTERM - value 15: signifies termination signal
  * SIGSTOP - value 17,19,23: signifies stop the process

* Pocesses:
    * daemon programs - programs with no ui that run in background
    * when a program launches another -parent process prodces child process
    * CTRL-s supsends processes while CNTRL-q resumes them
    * Process related commands:
        * top - display tasks
        * jobs - diplay a list of active jobs
        * bg - Put job in background
        * fg - put job in foreground
        * kill- send signall to process
        * killall - kill processes by name
        * shutdown - reboot or shutdown system
        * ps - prints snapshot of processes currently running
            * x - prints all processes regardless of terminal controlled by
            * aux - more info than x

* Shell Variables:
    * \$# - variable that stores the number of arguments
    * \$@ - variable that stores the paramaters that were passed
    * \$? - variable that stores the last success code
    * \$\$ - varibale that stores the proccess id (pid) of a program

* Permissions
    * chmod u=wrx,go= (file) - changes permisions so you can only run it

* Control Operators:
    * command1 && command2 - command2 executed if command1 returns status 0
    * command1 || command2 - command2 executed if command1 returns nonzero status

* Temporary Files:
    * use TEMP_FILE=\$TEMP_DIR/(program name).\$\$.\$RANDOM to name your temporary files

* Simple Variable cutting:
    
    ```
    var="apple orange"

    # This command will print 'apple'
    echo "$var%% *}"

    # This command will print 'orange'
    echo "${var##* }


    ```

## hyperfine graphing project:

```sh
line=$(grep clear --export-markown preformance.txt)
linea=${1ine#*| }
lineb=$(linea% |*}
mean=$lineb
relative=$lineb
for i in {0..3}; do
  if [ $i -eq 1 ]
  
  fi
  mean=${mean% |*}
  relative=${relative#*| }

done


```


