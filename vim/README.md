---
title: commands
---


# Vim Commands:

# ***try sudo -E vi ~/.vimrc for plugings***

* tab commands
    *  LtL - nex tab
    *  Ltn - new tab
* i (insert)
* motion  D - Deletes line
* gf - find and edit file under cursor
* set nohlsearch same thing but permanent
* *. in command mode repeats last command*
* :so % - restarts vimrc file without having to close+reopen
* m + (character) - set mark in file
* backtick + mark charcter jumps to that file at that location
* f & t - jump to character on line. f at the char, t before char
* <s-*> - in normal mode searches for the word under cursor
* <c-w>[w, -, +, etc..] - key for manipulating windows
* :next/:previous - moving between open vi files

 ##To change every occurrence of a character string between two lines,

* :#,#s/old/new/g    where #,# are the line numbers of the range of lines where the substitution is to be done.
* :%s/old/new/g      to change every occurrence in the whole file.
* :%s/old/new/gc     to find every occurrence in the whole file, with a prompt whether to substitute or not.
* use /< and /> to match the beginning and end of a word

```
Examples:
:s/a\|b/xxx\0xxx/g		 modifies "a b"	     to "xxxaxxx xxxbxxx"
:s/\([abc]\)\([efg]\)/\2\1/g	 modifies "af fa bg" to "fa fa gb"
:s/abcde/abc^Mde/		 modifies "abcde"    to "abc", "de" (two lines)
:s/$/\^M/			 modifies "abcde"    to "abcde^M"
:s/\w\+/\u\0/g		 modifies "bla bla"  to "Bla Bla"

```

---
title: settings
---

# Vim Settings


* settings should be saved in ~/.vimrc
* set nu - nonu -- line numbers
* :noh turns of highlighting until the next search

## Mapping:

* nunmap - defaults a keymap
* *always* use the noremap version of the commands
* onoremap - used for movement remaps

## Text Objects

* iw => "inner word"(works from anywhere in a word)
* it => "inner tag"(the contents of an HTML tag)
* i" => "inner quotes"
* ip => "inner paragraph"
* as => "a sentence"

---
title: plugins
---

# Vim Plugins

## vimwiki

Basic key bindings:
* <Leader>ww -- Open default wiki index file.
* <Leader>wt -- Open default wiki index file in a new tab.
* <Leader>ws -- Select and open wiki index file.
* <Leader>wd -- Delete wiki file you are in.
* <Leader>wr -- Rename wiki file you are in.
* <Enter> -- Follow/Create wiki link.
* <Shift-Enter> -- Split and follow/create wiki link.
* <Ctrl-Enter> -- Vertical split and follow/create wiki link.
* <Backspace> -- Go back to parent(previous) wiki link.
* <Tab> -- Find next wiki link.
* <Shift-Tab> -- Find previous wiki link.






