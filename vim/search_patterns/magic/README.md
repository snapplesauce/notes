---
title: magic
---
# magic

Wether a character in a regex is taken literally or not depends if the magic option is set

You can set if a regex is magic or not inline with \v, \V, \m, \M

* /m - 'magic': acts as if magic is set
* /M - 'nomagic': acts as is nomagic is set
* /v - 'very magic': means that [0-9a-zA-Z] and '_' all have speacial meaning
* /V - 'very nomagic'" means that only the backslsh character has speacial meaning

## example table from the [vim user manual](http://vimdoc.sourceforge.net/htmldoc/pattern.html#/magic):

 /v     /m   /M    /V    matches
----- ----- ----- ----- ------------------------------------------------
  .     .     \.   \.     matches any character
 \*    \*     \*   \*     any number of the previous atom
