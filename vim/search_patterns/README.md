---
title: search_patterns
---
# search patterns

see [regular expressions](../../regex/README.md)


## patterns:

* \(p1\|p2\) - matches either p1 or p2
* \w - matches the word text object. Same as [a-zA-Z0-9]
* \s - matches whitespace
* \n - matches newline character
* P* - pattern zero or more times
* p\+ - pattern one or more times
* \= - expands the value of an expression for substitution
* submatch(0) - refrences the match
* /v - [very magic](./very_magic/README.md)


## example vim search replacement using patterns

```vim
Thompson, George
Bitties, Tigole

:%s//([^,]\), \(.\)/ \2 \1/

 ```
               \([^,]*\), \(.*\)

   The first part between \( \) matches "Last" \(     \)
       match anything but a comma              [^,]
       any number of times                     *
   matches ", " literally                      ,
   The second part between \( \) matches "First"      \(  \)
       any character                     .
       any number of times               *

## example of regex that capitalizes words in a sentence for a whole file

credit to u/Atralb on reddit

```vim
:%s/\(\%^\s*\w\|\.\(\s*\w\|\s\+\w\)\)/\=toupper(submatch(0))/g

" refined version
:%s/\v(%^\_s*|[.!?]\_s+)\zs\a/\u&/g

```









