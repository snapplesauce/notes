---
title: commands
---


# Vim Commands:

* tab commands
    *  LtL - nex tab
    *  Ltn - new tab
* i (insert)
* motion  D - Deletes line
* gf - find and edit file under cursor
* set nohlsearch same thing but permanent
* *. in command mode repeats last command*
* :so % - restarts vimrc file without having to close+reopen
* m + (character) - set mark in file
* backtick + mark charcter jumps to that file at that location
* f & t - jump to character on line. f at the char, t before char
* <s-*> - in normal mode searches for the word under cursor
* <c-w>[w, -, +, etc..] - key for manipulating windows
* :next/:previous - moving between open vi files
* [I - list all lines in the  included files that contain the word under the cursor, start searching at beginning of current file


 ##To change evst all lines found in current and

* :#,#s/old/new/g    where #,# are the line numbers of the range of lines where the substitution is to be done.
* :%s/old/new/g      to change every occurrence in the whole file.
* :%s/old/new/gc     to find every occurrence in the whole file, with a prompt whether to substitute or not.
* use /< and /> to match the beginning and end of a word

```
Examples:
:s/a\|b/xxx\0xxx/g		 modifies "a b"	     to "xxxaxxx xxxbxxx"
:s/\([abc]\)\([efg]\)/\2\1/g	 modifies "af fa bg" to "fa fa gb"
:s/abcde/abc^Mde/		 modifies "abcde"    to "abc", "de" (two lines)
:s/$/\^M/			 modifies "abcde"    to "abcde^M"
:s/\w\+/\u\0/g		 modifies "bla bla"  to "Bla Bla"

```

