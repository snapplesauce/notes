---
title: settings
---

# Vim Settings


* settings should be saved in ~/.vimrc
* set nu - nonu -- line numbers
* :noh turns of highlighting until the next search

## Mapping:

* nunmap - defaults a keymap
* *always* use the noremap version of the commands
* onoremap - used for movement remaps

## Text Objects

* iw => "inner word"(works from anywhere in a word)
* it => "inner tag"(the contents of an HTML tag)
* i" => "inner quotes"
* ip => "inner paragraph"
* as => "a sentence"

