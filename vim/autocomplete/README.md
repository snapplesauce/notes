---
title: autocomplete
---
# Autocomplete

you can use <c-n> and <c-p> to cycle back and forth in list.


## useful commands:

* <c-x><c-n> use ***JUST*** this file to autocomplete
* <c-f> to autocomplete filenames
* <c-x>] to autocomplete tags
* <c-n> autocomplete from the 'complete option
