---
title: regex
---
# Regular expressions:

see  [vim search pattern](../vim/search_patterns/README.md)

# patterns

    * \* - matches any charcter(and number?)
    * ^ matches beginning
    * $ matches end




```
          ## Example vim search replacement using patterns

              \([^,]*\), \(.*\)

  The first part between \( \) matches "Last"	\(     \)
      match anything but a comma			        [^,]
      any number of times				              *
  matches ", " literally					            ,
  The second part between \( \) matches "First"		   \(  \)
      any character					            .
      any number of times					      *
```

