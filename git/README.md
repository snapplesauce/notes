---
title: git
---


## git
  * ssh
  * ~/.gitconfig
  * git remote add git@gitlab.com:snapplesauce/eightball.git

## git commands:

* git status
* git add .
* git push
* git pull
* git commit -m
* git clone
* git stash
* git log

