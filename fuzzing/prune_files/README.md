---
title: prune_files
---

# prune_files

find * | xargs file | grep -v ELF | cut -d: -f1 | xargs rm
