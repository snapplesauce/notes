---
title: demux
---

# demux
```
**
 * Demultiplexor:
 * {a, b} = {in, 0} if sel == 0
 *          {0, in} if sel == 1
 */

CHIP DMux {
    IN in, sel;
    OUT a, b;

    PARTS:

    Not(in=sel, out=nSel);
    And(a=in, b=nSel, out=a);
    And(a=in, b=sel,  out=b);


--------------------------------------------------------------------



/**
 * 4-way demultiplexor:
 * {a, b, c, d} = {in, 0, 0, 0} if sel == 00
 *                {0, in, 0, 0} if sel == 01
 *                {0, 0, in, 0} if sel == 10
 *                {0, 0, 0, in} if sel == 11
 */

CHIP DMux4Way {
    IN in, sel[2];
    OUT a, b, c, d;

    PARTS:
    DMux(in=in, sel=sel[1],a=ao,b=bo);
    DMux(in=ao, sel=sel[0],a=a,b=b);
    DMux(in=bo, sel=sel[0],a=c,b=d);


-----------------------------------------------------------------------

**
 * 8-way demultiplexor:
 * {a, b, c, d, e, f, g, h} = {in, 0, 0, 0, 0, 0, 0, 0} if sel == 000
 *                            {0, in, 0, 0, 0, 0, 0, 0} if sel == 001
 *                            etc.
 *                            {0, 0, 0, 0, 0, 0, 0, in} if sel == 111
 */

CHIP DMux8Way {
    IN in, sel[3];
    OUT a, b, c, d, e, f, g, h;

    PARTS:

    DMux(in=in, sel=sel[2],a=ao,b=bo);
    
    DMux(in=ao, sel=sel[1],a=aoo,b=boo);
    DMux(in=bo, sel=sel[1],a=coo, b=doo);

    DMux(in=aoo, sel=sel[0],a=a,b=b);
    DMux(in=boo, sel=sel[0],a=c,b=d);
    DMux(in=coo, sel=sel[0],a=e,b=f);
    DMux(in=doo, sel=sel[0],a=g,b=h);


}
```



