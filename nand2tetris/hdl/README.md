---
title: hdl
---

# Hardware Description Language


```hdl
CHIP xor {
    IN a, b;
    OUT out;
    
    
    PARTS:
    Not (in=a, out=nota) 
    Not (in=b, out=notb) 
    And (a=a, b=notb, out=aAndNotb) 
    And (a=nota, b=b, out=notaAndb) 
    Or  (a=aAndNotb, b=notaAndb, out=out) 
}
```
You need you use the interface that is unique to the chip:
* Not(in= ,out= ), And(a= ,b=, out= ), Or(a=, b=, out= )

Connections: partName(a=a,...), partName(...,out=out)
