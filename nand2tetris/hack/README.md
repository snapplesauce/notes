---
title: hack
---

# hack

Hack is our computers assembly language. Its only aware of two address locations, referred to as instruction memory and data memory. These memories have 15-bit address spaces and are 16 bit wide. It can only have one program loaded at a time.


The registers we can interact with are D and A. They are 16-bits wide. D is used to sore data ***only***, while A has a double purpose as a data register ***and*** as an address register. You can't fit both an instruction and an address in a 16-bit wide call so first you move to the address and then you commit the instructions.

Data memory is accessed through the A register, and the convention is that **M** refers to it's(A registers) current value.To use jump instructions, it's convention to set A to the specified address first and then issue the jump command.

## A-instruction: @value
A-instruction( '***@***') has the computer store the value provided. It has three different use cases:
The semantics of the symbolic instruction is *dest* =comp;jump:
    1. Only way to create a constant while  under program control
    1. Sets up C-instructions by loading specified address into the A register.
    1. Sets up by loading specified address into the A register.




```
                   value(v = 0 or 1)
            ____________________________
           |                            |
Binary:  0 V V V  V V V V  V V V V  V V V 
```


## C-instruction

* both dest *or* jump fields may be empty
* if dest is empty, the instruction doesn't contain an '='
* if jump  is empty, the instruction doesn't contain an ';'

* most significant bit (farthest left bit)  is the C-instruction code.
* the next to bits are not relevant in any way.
* c1 to c6 are 

## The Computation Specification

* the 7 bit pattern of an A bit followed by 6 c bits
* the language specifies 28 functions of the 128 that it can potentially compute

```
       c-insruction
        |   ___________comp______  _______dest jump___
        |  |                     ||         |  |      |
Binary: 111a     c1 c2 c3 c4  c5 c6d d1 d2   d3 j1 j2 j3  

```      
## The Destination Specifat
* can store the value that was calculated by the computation part
* this location is encoded in the 3 bit dest part
* d1 codes whether or not to store the value in the A register, and likewise with d2 and the D register
* the last bit(d3) codes whether to load the value into M(Memory[A])
      
      
