---
title: multiplexor
---

# multiplexors
```
   |\  
   | \ 
a- |  \
   |   \ 
   |    \          
   |     \ 
   |Mux   \
   |      / ---out
   |     / 
b- |    /  
   |   /
   |  /| 
   | / |
      sel
      
      
      
  if (sel==0)
    out=a
  else
    out=b
    
 ----------------------------------------------------------------------- 

chip AndMuxOr {
    IN a, b, sel;
    OUT out;
    
    PARTS:
  
    And (a=a, b=b, out=andOut);
    or (a=a, b=b, out=orOut);
    Mux (a=andOut, b=orOut, sel=sel, out=out);  
```  

 sel   out
----- -----
 0      a
 1      b
