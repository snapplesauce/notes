---
title: radare2
---

# radare2

*e??<query> - info on commands and config options

## Panel mode

* get to it with command v
* m - access menu
* p - change views
* enter - zoom in and out from pannels
* w - enter window mode

## Registers

* drr - telescope registers
* 


## Stack

* pxr(print hexadecimal references) @ esp - stack telescoping
* pxa @ <stack pointer> - show annotated hexdump of stack
* pxw @ <stack pointer> - show hex word dump
* pxw @ <stack pointer> - show hex quadword dump
* ad@r:SP - analyze stack data

