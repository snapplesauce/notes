---
title: case_until
---

# Case and Until examples:

[test lynk](../shift/README.md)


```sh
----------------

case word in

  [[:lower:]] | [[:upper:]] ) echo "you typed a letter"

  [0-9] )                     echo "you typed a digit"

  * )                         echo "you typed anythings else"

esac


----------------

until [ CONDITION ]
;
do


  body

done

```
