---
title: bash
---

## permisions:


 | Owner | Group | World |
 |------ |-------|-------|
 |   wrx |   wrx |   wrx |



| Octal | Binary | File Mode |
|:-----:|:------:|:---------:|
|   0   |   000  |    ---    |
|   1   |   001  |    --x    |
|   2   |   010  |    -w-    |
|   3   |   011  |    -wx    |
|   4   |   100  |    r--    |
|   5   |   101  |    r-x    |
|   6   |   110  |    rw-    |
|   7   |   111  |    rwx    |



## bash if, for while, and case statements


```sh

if [[ ]]  #double brackets for bash single for sh
then

  body

fi

----------------

while [[ ]]
do

  body

done

-----------------

for i in {range}
do

  body

done


----------------

case word in

  [[:lower:]] | [[:upper:]] ) echo "you typed a letter"

  [0-9] )                     echo "you typed a digit"

  * )                         echo "you typed anythings else"

esac


----------------

until [ CONDITION ]
do


  body

done

```

## bash Error Exit Function


```sh


# An error exit function

PROGNAME=$(basename $0)

error_exit()
{
 # argument is string description of error


  echo "${PROGNAME}: ${1:-"Unknown Error")" 1>&2
  exit 1
}

# Using error_exit

if cd $some_directory; then
  rm *
else
  error_exit "$LINENO:Cannot change directory!  Aborting."
fi

# Implementation using || operator

command || error_exit "error message"

```


## Example of a cleanup function and using trap



```sh

# Program to print a text file with headers and footers

TEMP_FILE=/tmp/printfile.txt

clean_up() {

  # Perform program exit housekeeping
  rm $TEMP_FILE
  exit
}

trap clean_up SIGHUP SIGINT SIGTERM

pr $1 > $TEMP_FILE

echo -n "Print file? [y/n]: "
read
if [ "$REPLY" = "y" ]; then
  lpr $TEMP_FILE
fi
clean_up


```

## Example of shift to increment parameters

```sh



echo "You start with $# positional parameters"

 # Loop until all parameters are used up
  while [ "$1" != "" ]; do
       echo "Parameter 1 equals $1"
            echo "You now have $# positional parameters"

                 # Shift all the parameters down by one
                      shift

                       done

```

