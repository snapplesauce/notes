---
title:Errors
---
## bash Error Exit Function


```sh


# An error exit function

PROGNAME=$(basename $0)

error_exit()
{
 # argument is string description of error


  echo "${PROGNAME}: ${1:-"Unknown Error")" 1>&2
  exit 1
}

# Using error_exit

if cd $some_directory; then
  rm *
else
  error_exit "$LINENO:Cannot change directory!  Aborting."
fi

# Implementation using || operator

command || error_exit "error message"

```

