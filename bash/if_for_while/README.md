---
title: if_for_while
---

## bash if, for while, and case statements


```sh

if [[ ]]  #double brackets for bash single for sh
then

  body

fi

----------------

while [[ ]]
do

  body

done

-----------------

for i in {range}
do

  body

done


```

