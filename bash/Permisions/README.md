---
Title: bash
title: Permisions
---

# permisions:


 | Owner | Group | World |
 |------ |-------|-------|
 |   wrx |   wrx |   wrx |



| Octal | Binary | File Mode |
|:-----:|:------:|:---------:|
|   0   |   000  |    ---    |
|   1   |   001  |    --x    |
|   2   |   010  |    -w-    |
|   3   |   011  |    -wx    |
|   4   |   100  |    r--    |
|   5   |   101  |    r-x    |
|   6   |   110  |    rw-    |
|   7   |   111  |    rwx    |


